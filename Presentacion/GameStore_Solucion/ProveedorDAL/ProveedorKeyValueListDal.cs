﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProveedorDAL
{
    public class ProveedorKeyValueListDal
    {
        /// <summary>
        /// Retorna una lista de identifificadores y nombre completo de Proveedors
        /// </summary>
        /// <param name="apellido">Apellido paterno de Proveedors</param>
        /// <returns></returns>
        public static ProveedorKeyValueList Get(string nombre)
        {
            ProveedorKeyValueList lista = new ProveedorKeyValueList();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idProveedor, razonSocial, telefono
                            FROM Proveedor
                            Where estado = 1 and razonSocial like @razonSocial";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@razonSocial", string.Format("%{0}%", nombre));
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    lista.Add(new ProveedorKeyValue()
                    {
                        IdProveedor = dr.GetInt32(0),
                        RazonSocial = dr.GetString(1),
                        Telefono = dr[2].ToString()
                    });
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
