﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProveedorDAL
{
    public class ProveedorDal
    {
        /// <summary>
        /// Inserta un Usuario a la base de datos 
        /// </summary>
        /// <param name="Proveedor"></param>
        public static void Insert(Proveedor Proveedor)
        {
            Operaciones.WriteLogsDebug("ProveedorDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un Proveedor"));

            SqlCommand command = null;

            //Consulta para insertar Proveedors
            string queryString = @"INSERT INTO Proveedor(razonSocial,telefono,estado)
                             VALUES(@razonSocial,@telefono,@estado)";

            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);                
                command.Parameters.AddWithValue("@razonSocial", Proveedor.RazonSocial);
                command.Parameters.AddWithValue("@telefono", Proveedor.Telefono);
                command.Parameters.AddWithValue("@estado", 1);

                OperacionesSql.ExecuteBasicCommand(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorDal", "Insertar", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar Proveedor"));
        }


        /// <summary>
        /// Actualiza Usuario de la base de datos
        /// </summary>
        /// <param name="Proveedor"></param>
        public static void Update(Proveedor Proveedor)
        {
            Operaciones.WriteLogsDebug("ProveedorDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Proveedor"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Proveedor SET razonSocial=@razonSocial,telefono=@telefono
                             WHERE idProveedor=@idProveedor";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idProveedor", Proveedor.IdProveedor);
                command.Parameters.AddWithValue("@razonSocial", Proveedor.RazonSocial);
                command.Parameters.AddWithValue("@telefono", Proveedor.Telefono);             
                
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorDal", "Actualizar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }



        /// <summary>
        /// Elimina Usuario de la base de datos
        /// </summary>
        /// <param name="idProveedor"></param>
        public static void SoftDelete(int idProveedor)
        {
            Operaciones.WriteLogsDebug("ProveedorDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Proveedor"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Proveedor SET estado=0
                                    WHERE idProveedor = @idProveedor";
            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idProveedor", idProveedor);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorDal", "Eliminar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }


        public static Proveedor Get(int id)
        {
            Proveedor res = new Proveedor();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idProveedor,razonSocial,telefono
                           FROM Proveedor 
                           WHERE idProveedor=@id AND estado = 1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Proveedor
                    {
                        IdProveedor = id,
                        RazonSocial = dr.GetString(1),
                        Telefono = dr.GetString(2)
                    };
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
    }
}
