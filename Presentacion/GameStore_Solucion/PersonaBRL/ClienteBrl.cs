﻿using GameStore.Common;
using GameStore.DAL_Persona;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BRL_Persona
{
    public class ClienteBrl
    {
        /// <summary>
        /// Método lógica de negocio para insertar un Cliente
        /// </summary>
        /// <param name="Cliente"></param>
        public static void Insert(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un Cliente"));

            try
            {
                ClienteDal.Insertar(cliente);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar Cliente"));

        }

        /// <summary>
        /// Método para obtener  un Cliente
        /// </summary>
        /// <param name="id">Identificado del Cliente </param>
        /// <returns>Cliente</returns>
        public static Cliente Get(Guid id)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Insertar", string.Format("{0} Info: {1}",
          DateTime.Now.ToString(),
          "Empezando a ejecutar el método lógica de negocio para obtener un Cliente"));
            Cliente Cliente = null;
            try
            {
                Cliente = ClienteDal.Obtener(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "obtener", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para obtener Cliente"));

            return Cliente;
        }


        /// <summary>
        /// Método lógica de negocio para crear un Cliente en la lógica de negocio
        /// </summary>
        /// <param name="Cliente"></param>
        public static void Update(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Empezando a ejecutar el método lógica de negocio para Actualizar un Cliente"));

            try
            {
                ClienteDal.Actualizar(cliente);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Actualizar Cliente"));
        }

        public static void SoftDelete(Guid idCliente)
        {
            Operaciones.WriteLogsDebug("ClienteBrl", "Eliminar", string.Format("{0} Info: {1}",
               DateTime.Now.ToString(),
               "Empezando a ejecutar el método lógica de negocio para Eliminar un Cliente"));

            try
            {
                ClienteDal.Eliminar(idCliente);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteBrl", "Eliminar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Actualizar Cliente"));
        }
    }
}
