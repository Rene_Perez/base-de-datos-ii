﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;
using GameStore.DAL_Persona;

namespace GameStore.BRL_Persona
{
    public class UsuarioBrl
    {
        /// <summary>
        /// método lógica de negocio para insertar un usuario
        /// </summary>
        /// <param name="usuario"></param>
        public static void Insertar(Usuario usuario)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un usuario"));

            try
            {
                UsuarioDal.Insertar(usuario);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar usuario"));

        }

        /// <summary>
        /// método lógica de negocio para actulizar un usuario
        /// </summary>
        /// <param name="usuario"></param>
        public static void Actualizar(Usuario usuario)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Actualizar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un usuario"));

            try
            {
                UsuarioDal.Actualizar(usuario);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para actualizar usuario"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un usuario
        /// </summary>
        /// <param name="usuario"></param>
        public static void Eliminar(Guid id)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Eliminar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Eliminar un usuario"));

            try
            {
                UsuarioDal.Eliminar(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioBrl", "Eliminar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Eliminar usuario"));
        }
        /// <summary>
        /// método lógica de negocio para eliminar un usuario
        /// </summary>
        /// <param name="usuario"></param>
        public static Usuario Obtener(Guid id)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un usuario"));

            try
            {
                return UsuarioDal.Obtener(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
        /// <summary>
        /// método lógica de negocio para eliminar un usuario
        /// </summary>
        /// <param name="usuario"></param>
        public static Usuario ObtenerSession(string nombreUsuario, string password)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un usuario"));

            try
            {
                return UsuarioDal.ObtenerSession(nombreUsuario, password);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }

        /// <summary>
        /// Metodo que realiza una llamada al metodo LOGIN de la clase UsuarioDAl para iniciar una sesion
        /// </summary>
        /// <param name="usuario"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        public static Usuario ObtenerIdUsuario(string nombreUsuario)
        {
            Operaciones.WriteLogsDebug("UsuarioBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un usuario"));

            try
            {
                return UsuarioDal.ObtenerIdUsuario(nombreUsuario);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }

        /// <summary>
        /// Validar Pasword
        /// Validar Pasword
        /// </summary>
        /// <param name="nombreUsuario"></param>
        /// <param name="contrsenia"></param>
        /// <returns></returns>

        public static void CambiarPassword(Usuario usuario)
        {
            UsuarioDal.CambiarPassword(usuario);
        }
    }
}
