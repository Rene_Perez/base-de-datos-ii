﻿using GameStore.Common;
using GameStore.DAL_Persona;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BRL_Persona
{
    public class EmpleadoKeyValueListBrl
    {
        
        public static EmpleadoKeyValueList Get(string nombre)
        {
            Operaciones.WriteLogsDebug("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un EmpleadoKeyValueList"));

            try
            {
                return EmpleadoKeyValueListDal.Get(nombre);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("EmpleadoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
    }
}
