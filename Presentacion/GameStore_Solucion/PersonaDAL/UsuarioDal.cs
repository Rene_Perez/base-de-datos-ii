﻿using System;
using System.Data;
using System.Data.SqlClient;
using GameStore.Common;
namespace GameStore.DAL_Persona
{
    public class UsuarioDal
    {
        /// <summary>
        /// Inserta un Usuario a la base de datos 
        /// </summary>
        /// <param name="usuario"></param>
        public static void Insertar(Usuario usuario)
        {
            Operaciones.WriteLogsDebug("UsuarioDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un usuario"));

            SqlCommand command = null;

            //Consulta para insertar usuarios
            string queryString = @"INSERT INTO Usuario(idUsuario,nombreUsuario,password,rol,estado,estadoPassword) 
                                    VALUES(@idUsuario,@nombreUsuario,@password,@rol,@estado,@estadoPassword)";
            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);
                command.Parameters.AddWithValue("@nombreUsuario", usuario.NombreUsuario);
                command.Parameters.AddWithValue("@password", usuario.Password);
                command.Parameters.AddWithValue("@password", "empleado");
                command.Parameters.AddWithValue("@estado", 1);
                command.Parameters.AddWithValue("@estadoPassword", 0);

                OperacionesSql.ExecuteBasicCommand(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioDal", "Insertar", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar usuario"));
        }

        /// <summary>
        /// Inserta un usaurio a la base de datos con transaccion
        /// </summary>
        /// <param name="usuario">Objeto usuario </param>
        /// <param name="transaccion">Objeto transaccion</param>
        /// <param name="conexion">Objeto conexion</param>
        public static void InsertarTransaccion(Usuario usuario, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("UsuarioDal", "InsertarTransaccion", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un usuario"));

            SqlCommand command = null;

            //Consulta para insertar usuarios
            string queryString = @"INSERT INTO Usuario(idUsuario,nombreUsuario,password,rol,estado,estadoPassword) 
                                    VALUES(@idUsuario,@nombreUsuario,@password,@rol,@estado,@estadoPassword)";
            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);
                command.Parameters.AddWithValue("@nombreUsuario", usuario.NombreUsuario);
                command.Parameters.AddWithValue("@password", usuario.Password);
                command.Parameters.AddWithValue("@rol", "empleado");
                command.Parameters.AddWithValue("@estado", 1);
                command.Parameters.AddWithValue("@estadoPassword", 0);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "InsertarTransaccion", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "InsertarTransaccion", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioDal", "InsertarTransaccion", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar usuario"));
        }

        /// <summary>
        /// Elimina Usuario de la base de datos
        /// </summary>
        /// <param name="idUsuario"></param>
        public static void Eliminar(Guid idUsuario)
        {
            Operaciones.WriteLogsDebug("UsuarioDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Usuario"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Usuario SET estado=0
                                    WHERE idUsuario = @idUsuario";
            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idUsuario", idUsuario);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioDal", "Eliminar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPersona"></param>
        public static void EliminarPorIdPersonaConTransaccion(Guid idPersona, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("UsuarioDal", "EliminarPorIdPersona", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Usuario"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Usuario SET estado=0
                                    WHERE idUsuario = (Select idUsuario FROM Persona WHERE idPersona = @idPersona)";
            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idPersona", idPersona);
                OperacionesSql.ExecuteBasicCommandWithTransaction(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "EliminarPorIdPersona", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "EliminarPorIdPersona", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioDal", "EliminarPorIdPersona", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }

        /// <summary>
        /// Actualiza Usuario de la base de datos
        /// </summary>
        /// <param name="Usuario"></param>
        public static void Actualizar(Usuario usuario)
        {
            Operaciones.WriteLogsDebug("UsuarioDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Usuario"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Usuario SET nombreUsuario=@nombreUsuario, password=@password
                                    WHERE idUsuario=@idUsuario";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@nombreUsuario", usuario.NombreUsuario);
                command.Parameters.AddWithValue("@password", usuario.Password);
                command.Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("UsuarioDal", "Actualizar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));

        }
        /// <summary>
        /// Obtiene un Usuario de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Usuario Obtener(Guid id)
        {
            Usuario res = new Usuario();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idUsuario,nombreUsuario,password,rol,estado FROM Usuarios WHERE idUsuario=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Usuario()
                    {
                        IdUsuario = dr.GetGuid(0),
                        NombreUsuario = dr.GetString(1),
                        Password = dr.GetString(2),
                        Rol = dr.GetString(3),
                        Estado = dr.GetByte(4)
                    };
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        /// <summary>
        /// Obtiene un Usuario de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Usuario ObtenerSession(string nombreUsuario, string password)
        {
            Usuario res = new Usuario();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idUsuario,nombreUsuario,rol
                             FROM Usuario 
                             WHERE nombreUsuario=@nombreUsuario AND password=@password AND estado=1 AND estadoPassword=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", nombreUsuario);
                cmd.Parameters.AddWithValue("@password", password);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Usuario()
                    {
                        IdUsuario = dr.GetGuid(0),
                        NombreUsuario = dr.GetString(1),
                        Rol = dr.GetString(2)
                    };
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        public static Usuario ObtenerIdUsuario(string nombreUsuario)
        {
            Usuario res = new Usuario();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idUsuario,nombreUsuario,password,estado,estadoPassword 
                             FROM Usuario 
                             WHERE nombreUsuario=@nombreUsuario and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreUsuario", nombreUsuario);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Usuario()
                    {
                        IdUsuario = dr.GetGuid(0),
                        NombreUsuario = dr.GetString(1),
                        Password = dr.GetString(2),
                        Estado = dr.GetByte(3),
                        EstadoPasswrod = dr.GetByte(4)
                    };
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("UsuarioDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

        /// <summary>
        /// Metodo que ejecuta una consulta UPDATE en la tabla Usuario para cambiar contraseña
        /// </summary>
        public static void CambiarPassword(Usuario usuario)
        {
            string query = @"UPDATE Usuario 
                             set password = @password, estadoPassword=1
                             WHERE idUsuario = @idUsuario";
            try
            {
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: Inicio del Metodo de Cambiar Contraseña de Empleado Usuario", DateTime.Now));
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@password", usuario.Password).SqlDbType = SqlDbType.VarChar;
                cmd.Parameters.AddWithValue("@idUsuario", usuario.IdUsuario);

                Methods.ExecuteBasicCommand(cmd);
                System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: Contraseña Cambiada, Nombre Empleado: {1}", DateTime.Now, Sesion.usuarioSesion));
            }
            catch (Exception ex)
            {

                System.Diagnostics.Debug.WriteLine(string.Format("{0} Info: Error al Cambiar Contraseña:  {1} Usuario: {2}", DateTime.Now, ex.Message, Sesion.usuarioSesion));
            }
        }


    }
}
