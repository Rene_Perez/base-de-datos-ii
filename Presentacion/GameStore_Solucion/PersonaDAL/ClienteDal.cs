﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL_Persona
{
    public class ClienteDal
    {
        /// <summary>
        /// Inserta una Cliente a la base de datos 
        /// </summary>
        /// <param name="Cliente"></param>
        public static void Insertar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un Cliente"));

            SqlCommand command = null;

            //Consulta para insertar Clientes
            string queryString = @"INSERT INTO Cliente(idPersona, puntos) 
                                   VALUES(@idCliente, @puntos)";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                //Inserto a la persona
                PersonaDal.InsertarConTransaccionNoUser(cliente as Persona, transaccion, conexion);

                //Inserto al Cliente
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idCliente", cliente.IdPersona);
                command.Parameters.AddWithValue("@puntos", cliente.Puntos);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                transaccion.Commit();

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Insertar", string.Format("{0} Error: {1} ",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            Operaciones.WriteLogsDebug("ClienteDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar Cliente"));
        }

        /// <summary>
        /// Metodo para obtener  un Cliente
        /// </summary>
        /// <param name="id">Identificado del Cliente </param>
        /// <returns>Cliente</returns>
        public static Cliente Obtener(Guid id)
        {
            Cliente cliente = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT c.idPersona, c.puntos
                             FROM Cliente c
                             INNER JOIN Persona p ON p.idPersona = c.idPersona
                             WHERE c.idPersona=@id and p.estado = 1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                cliente = PersonaDal.ObtenerCliente(id);
                while (dr.Read())
                {
                    cliente.IdPersona = dr.GetGuid(0);
                    cliente.Puntos = dr.GetInt16(1);
                }

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Obtenet", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Obtenet", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return cliente;
        }

        /// <summary>
        /// Método para actulizar a un Cliente
        /// </summary>
        /// <param name="Cliente"></param>
        public static void Actualizar(Cliente cliente)
        {
            Operaciones.WriteLogsDebug("ClienteDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Persona"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Cliente SET puntos=@puntos
                                    WHERE idPersona=@idPersona";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@puntos", cliente.Puntos);
                command.Parameters.AddWithValue("@idPersona", cliente.IdPersona);

                //Actualizo a la persona
                PersonaDal.Actualizar(cliente as Persona);

                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteDal", "Actualizar", string.Format("{0}  Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));

        }

        /// <summary>
        /// Método para eliminar a un Cliente
        /// </summary>
        /// <param name="Cliente"></param>
        public static void Eliminar(Guid idCliente)
        {
            Operaciones.WriteLogsDebug("ClienteDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Persona"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Cliente SET idPersona = @id
                                    WHERE idPersona=@id";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@id", idCliente);

                //Elimino a la persona
                PersonaDal.EliminarConTransaccion(idCliente, transaccion, conexion);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Actualizar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ClienteDal", "Actualizar", string.Format("{0}  Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));

        }
    }
}
