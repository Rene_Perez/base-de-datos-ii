﻿using System;
using System.Data.SqlClient;
using GameStore.Common;

namespace GameStore.DAL_Persona
{
    /// <summary>
    /// Clase que sirve para interactuar con la base de datos
    /// </summary>
    public class PersonaDal
    {
        /// <summary>
        /// Inserta una Persona a la base de datos 
        /// </summary>
        /// <param name="persona"></param>
        public static void Insertar(Persona persona)
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un persona"));

            SqlCommand command = null;

            //Consulta para insertar personas
            string queryString = @"INSERT INTO Persona(idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, estado) 
                                   VALUES(@idPersona, @ci, @nombres, @primerApellido, @segundoApellido, @sexo, @fechaNacimiento, @direccion, @estado)";
            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                //Abro la conexion a la base de datos
                conexion.Open();

                //Inicio la transaccion
                transaccion = conexion.BeginTransaction();

                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                if (persona.Usuario != null)
                {
                    UsuarioDal.InsertarTransaccion(persona.Usuario, transaccion, conexion);
                    command.Parameters.AddWithValue("@idUsuario", persona.Usuario.IdUsuario);
                }
                else
                {
                    command.Parameters.AddWithValue("@idUsuario", null);
                }

                command.Parameters.AddWithValue("@idPersona", persona.IdPersona);
                command.Parameters.AddWithValue("@ci", persona.Ci);
                command.Parameters.AddWithValue("@nombres", persona.Nombres);
                command.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
                command.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
                command.Parameters.AddWithValue("@sexo", persona.Sexo);
                command.Parameters.AddWithValue("@fechaNacimiento", persona.FechaNacimiento);
                command.Parameters.AddWithValue("@direccion", persona.Direccion);
                command.Parameters.AddWithValue("@estado", 1);
                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                ////Insertar telefonos
                //foreach (Telefono telf in persona.Telefonos)
                //{
                //    TelefonoDal.InsertarConTransaccion(telf, transaccion, conexion);
                //}
                transaccion.Commit();

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                conexion.Close();
            }
            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar persona"));
        }

        /// <summary>
        /// Inserta una Persona a la base de datos 
        /// </summary>
        /// <param name="persona"></param>
        public static void InsertarConTransaccion(Persona persona, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un persona"));

            SqlCommand command = null;

            //Consulta para insertar personas
            string queryString = @"INSERT INTO Persona(idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, estado, idUsuario) 
                                   VALUES(@idPersona, @ci, @nombres, @primerApellido, @segundoApellido, @sexo, @fechaNacimiento, @direccion, @estado,@idUsuario)";

            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                if (persona.Usuario != null)
                {
                    UsuarioDal.InsertarTransaccion(persona.Usuario, transaccion, conexion);
                    command.Parameters.AddWithValue("@idUsuario", persona.Usuario.IdUsuario);
                }
                else
                {
                    command.Parameters.AddWithValue("@idUsuario", null);
                }

                command.Parameters.AddWithValue("@idPersona", persona.IdPersona);
                command.Parameters.AddWithValue("@ci", persona.Ci);
                command.Parameters.AddWithValue("@nombres", persona.Nombres);
                command.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
                command.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
                command.Parameters.AddWithValue("@sexo", persona.Sexo);
                command.Parameters.AddWithValue("@fechaNacimiento", persona.FechaNacimiento);
                command.Parameters.AddWithValue("@direccion", persona.Direccion);
                command.Parameters.AddWithValue("@estado", 1);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                ////Insertar telefonos
                //foreach (Telefono telf in persona.Telefonos)
                //{
                //    TelefonoDal.InsertarConTransaccion(telf, transaccion, conexion);
                //}
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar persona"));
        }

        /// <summary>
        /// Inserta una Persona a la base de datos 
        /// </summary>
        /// <param name="persona"></param>
        public static void InsertarConTransaccionNoUser(Persona persona, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un persona"));

            SqlCommand command = null;

            //Consulta para insertar personas
            string queryString = @"INSERT INTO Persona(idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, estado) 
                                   VALUES(@idPersona, @ci, @nombres, @primerApellido, @segundoApellido, @sexo, @fechaNacimiento, @direccion, @estado)";

            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);

                command.Parameters.AddWithValue("@idPersona", persona.IdPersona);
                command.Parameters.AddWithValue("@ci", persona.Ci);
                command.Parameters.AddWithValue("@nombres", persona.Nombres);
                command.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
                command.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
                command.Parameters.AddWithValue("@sexo", persona.Sexo);
                command.Parameters.AddWithValue("@fechaNacimiento", persona.FechaNacimiento);
                command.Parameters.AddWithValue("@direccion", persona.Direccion);
                command.Parameters.AddWithValue("@estado", (byte)1);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("PersonaDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar persona"));
        }
        /// <summary>
        /// Elimina Persona de la base de datos
        /// </summary>
        /// <param name="idPersona"></param>
        public static void EliminarConTransaccion(Guid idPersona, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Persona"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Persona SET estado=0
                                    WHERE idPersona = @idPersona";
            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idPersona", idPersona);
                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

                //elimina al usuario
                UsuarioDal.EliminarPorIdPersonaConTransaccion(idPersona, transaccion, conexion);

                ////Eliminar telefonos
                //TelefonoDal.EliminarPorIdPersonaConTransaccion(idPersona, transaccion, conexion);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }

        /// <summary>
        /// Actualiza Persona de la base de datos
        /// </summary>
        /// <param name="Persona"></param>
        public static void Actualizar(Persona persona)
        {
            Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Persona"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Personas SET ci=@ci, nombres=@nombres, primerApellido=@primerApellido, segundoApellido=@segundoApellido, sexo=@sexo, fechaNacimiento=@fechaNacimiento, direccion=@direccion
                                   WHERE idPersona=@idPersona";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idPersona", persona.IdPersona);
                command.Parameters.AddWithValue("@ci", persona.Ci);
                command.Parameters.AddWithValue("@nombres", persona.Nombres);
                command.Parameters.AddWithValue("@primerApellido", persona.PrimerApellido);
                command.Parameters.AddWithValue("@segundoApellido", persona.SegundoApellido);
                command.Parameters.AddWithValue("@sexo", persona.Sexo);
                command.Parameters.AddWithValue("@fechaNacimiento", persona.FechaNacimiento);
                command.Parameters.AddWithValue("@direccion", persona.Direccion);

                //Actualizo al usuario
                if (persona.Usuario != null)
                {
                    command.Parameters.AddWithValue("@idUsuario", persona.Usuario.IdUsuario);
                    UsuarioDal.Actualizar(persona.Usuario);
                }
                else
                {
                    command.Parameters.AddWithValue("@idUsuario", DBNull.Value);
                }

                ////Actualizo los telefonos
                //foreach (var telefono in persona.Telefonos)
                //{
                //    TelefonoDal.Actualizar(telefono);
                //}

                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Eliminar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("PersonaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));

        }

        /// <summary>
        /// Obtiene un Persona de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Persona Obtener(Guid id)
        {
            Persona persona = new Persona();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, idUsuario, estado 
                             FROM Persona 
                             WHERE idPersona=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    persona = new Persona()
                    {
                        IdPersona = dr.GetGuid(0),
                        Ci = dr.GetString(1),
                        Nombres = dr.GetString(2),
                        PrimerApellido = dr.GetString(3),
                        SegundoApellido = dr.GetString(4),
                        Sexo = dr.GetByte(5),
                        FechaNacimiento = dr.GetDateTime(6),
                        Direccion = dr.GetString(7),
                        Usuario = UsuarioDal.Obtener(dr.GetGuid(8)),
                        Estado = dr.GetByte(9)
                    };

                    //persona.Telefonos = TelefonosListDal.Obtener(persona.IdPersona);

                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return persona;
        }

        /// <summary>
        /// Obtiene un Persona de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Guid ObtenerIdUsuario(Guid idPersona)
        {
            Guid idUsuario = Guid.Empty;
            SqlCommand cmd = null;
            string query = @"SELECT idUsuario 
                             FROM Persona 
                             WHERE idPersona=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", idPersona);
                idUsuario = new Guid(OperacionesSql.ExcecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return idUsuario;
        }

        /// <summary>
        /// Obtiene un Persona de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Empleado ObtenerEmpleado(Guid id)
        {
            Empleado persona = new Empleado();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, idUsuario, estado 
                             FROM Persona 
                             WHERE idPersona=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    persona = new Empleado()
                    {
                        IdPersona = dr.GetGuid(0),
                        Ci = dr.GetString(1),
                        Nombres = dr.GetString(2),
                        PrimerApellido = dr.GetString(3),
                        SegundoApellido = dr.GetString(4),
                        Sexo = dr.GetByte(5),
                        FechaNacimiento = dr.GetDateTime(6),
                        Direccion = dr.GetString(7),
                        Usuario = UsuarioDal.Obtener(dr.GetGuid(8)),
                        Estado = dr.GetByte(9)
                    };

                    //persona.Telefonos = TelefonosListDal.Obtener(persona.IdPersona);

                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return persona;
        }
        /// <summary>
        /// Obtiene un Persona de la base de datos
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Cliente ObtenerCliente(Guid id)
        {
            Cliente persona = new Cliente();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idPersona, ci, nombres, primerApellido, segundoApellido, sexo, fechaNacimiento, direccion, estado 
                             FROM Persona 
                             WHERE idPersona=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    persona = new Cliente()
                    {
                        IdPersona = id,
                        Ci = dr.GetString(1),
                        Nombres = dr.GetString(2),
                        PrimerApellido = dr.GetString(3),
                        SegundoApellido = dr.GetString(4),
                        Sexo = dr.GetByte(5),
                        FechaNacimiento = dr.GetDateTime(6),
                        Direccion = dr.GetString(7),
                        Estado = dr.GetByte(8)
                    };

                    //persona.Telefonos = TelefonosListDal.Obtener(persona.IdPersona);

                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return persona;
        }
    }
}