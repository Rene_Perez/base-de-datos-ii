﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;

namespace GameStore.DAL_Producto
{
    public class ProductoKeyValueListDal
    {
        /// <summary>
        /// Retorna una lista de identifificadores y nombre completo de Productos
        /// </summary>
        /// <param name="apellido">Apellido paterno de Productos</param>
        /// <returns></returns>
        public static ProductoKeyValueList Get(string nombre)
        {
            ProductoKeyValueList lista = new ProductoKeyValueList();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idProducto, nombre, precio, stock
                            FROM Producto
                            Where estado = 1 and nombre like @nombre";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombre", string.Format("%{0}%", nombre));
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    lista.Add(new ProductoKeyValue()
                    {
                        IdProducto = dr.GetInt32(0),
                        Nombre = dr.GetString(1),
                        Precio = SqlMoney.Parse(dr[2].ToString()),
                        Stock = dr.GetInt32(3)
                    });
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
