﻿using System;

namespace GameStore.Common
{
    public class Usuario
    {
        public Guid IdUsuario { get; set; }
        public string NombreUsuario { get; set; }
        public string Password { get; set; }
        public string Rol { get; set; }
        public byte Estado { get; set; }
        public byte EstadoPasswrod { get; set; }
    }
}
