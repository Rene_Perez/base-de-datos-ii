﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class ProductoKeyValue
    {
        public int IdProducto { get; set; }
        public string Nombre { get; set; }
        public SqlMoney Precio { get; set; }
        public int Stock { get; set; }
    }
}
