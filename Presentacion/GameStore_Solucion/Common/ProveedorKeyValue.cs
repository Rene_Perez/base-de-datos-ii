﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class ProveedorKeyValue
    {
        public int IdProveedor { get; set; }
        public string RazonSocial { get; set; }
        public string Telefono { get; set; }        
    }
}
