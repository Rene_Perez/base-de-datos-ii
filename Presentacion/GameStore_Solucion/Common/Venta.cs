﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class Venta
    {
        public Guid IdVenta { get; set; }
        public SqlMoney TotalVenta { get; set; }
        public DateTime Fecha { get; set; }
        public Guid IdEmpleado { get; set; }
        public Guid IdCliente { get; set; }
        public List<DetalleVenta> DetalleVenta { get; set; }
    }
}
