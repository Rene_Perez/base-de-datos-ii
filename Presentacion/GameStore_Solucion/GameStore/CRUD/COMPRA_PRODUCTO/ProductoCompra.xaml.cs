﻿using System;
using System.Windows;
using System.Windows.Controls;
using GameStore.BRL_Producto;
using System.Data;
using GameStore.Common;
using System.Data.SqlTypes;
using System.Collections.Generic;
using CompraBRL;

namespace GameStore.CRUD.COMPRA_PRODUCTO
{
    /// <summary>
    /// Lógica de interacción para ProductoCompra.xaml
    /// </summary>
    public partial class ProductoCompra : Window
    {
        List<DetalleCompra> detalles = new List<DetalleCompra>();
        Guid idCompra;
        Producto producto;
        public ProductoCompra()
        {
            InitializeComponent();
            idCompra = Guid.NewGuid();
        }

        private void BtnRealizarCompra_Click(object sender, RoutedEventArgs e)
        {

            SqlMoney total = 0;
            foreach (DetalleCompra detalle in detalles)
            {
                total += detalle.PrecioUnitario;
            }
            Compra Compra = new Compra()
            {
                DetalleCompra = detalles,
                TotalCompra = total,
                IdEmpleado = Guid.Parse(txtEmpleado.Text),
                Fecha = DateTime.Parse(DateTime.Now.ToShortDateString()),
                IdCompra = idCompra,
                IdProveedor = int.Parse(txtProveedor.Text)
            };
            MessageBox.Show("Precio Total: " + Compra.TotalCompra + "\nFecha: " + Compra.Fecha);
            try
            {
                MessageBox.Show("Compra realizada exitosamente", "Advertencia", MessageBoxButton.OKCancel);
                CompraBrl.Insert(Compra);
                detalles = new List<DetalleCompra>();
                dgLista.ItemsSource = null;
            }
            catch (Exception)
            {

                throw;
            }
            RefrescarProductos();
        }


        public void RefrescarProductos()
        {
            dgProductos.ItemsSource = ProductoKeyValueListBrl.Get(txtGetProductos.Text.ToString());
            dgProductos.DisplayMemberPath = "Nombre";
            dgProductos.SelectedValuePath = "IdProducto";
        }

        public void RefrescarDetalles()
        {
            dgLista.ItemsSource = null;
            dgLista.ItemsSource = detalles;
        }

        private void BtnAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            producto = ProductoBrl.Get(int.Parse(dgProductos.SelectedValue.ToString()));
            detalles.Add(new DetalleCompra()
            {
                IdProducto = producto.IdProducto,
                CantidadProducto = int.Parse(txtCantidad.Text),
                PrecioUnitario = producto.Precio * int.Parse(txtCantidad.Text),
                IdCompra = idCompra
            });

            RefrescarDetalles();
        }


        private void TxtGetProductos_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarProductos();
        }

        private void BtnAgregarProveedor_Click(object sender, RoutedEventArgs e)
        {
            CRUD_PROVEEDOR.ProveedorCrud ventana = new CRUD_PROVEEDOR.ProveedorCrud();
            ventana.Owner = this;
            ventana.Show();
        }

        private void BtnAgregarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            CRUD_EMPLEADO.EmpleadoCrud ventana = new CRUD_EMPLEADO.EmpleadoCrud();
            ventana.Owner = this;
            ventana.Show();
        }
    }
}

