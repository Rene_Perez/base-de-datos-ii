﻿using GameStore.Common;
using ProveedorBRL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameStore.CRUD.CRUD_PROVEEDOR
{
    /// <summary>
    /// Lógica de interacción para ProveedorCrud.xaml
    /// </summary>
    public partial class ProveedorCrud : Window
    {
        public ProveedorCrud()
        {
            InitializeComponent();
        }

        public void RefrescarProductos()
        {
            lbxDatos.ItemsSource = ProveedorKeyValueListBrl.Get(txtGet.Text.ToString());
            lbxDatos.DisplayMemberPath = "razonSocial";
            lbxDatos.SelectedValuePath = "IdProveedor";
        }

        private void TxtGet_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarProductos();
        }

        private void BtnInsert_Click(object sender, RoutedEventArgs e)
        {
            ProveedorInsert proveedorInsertVentana = new ProveedorInsert();
            proveedorInsertVentana.Owner = this;
            proveedorInsertVentana.ShowDialog();
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lbxDatos.SelectedValue != null)
                {
                    ProveedorBrl.SoftDelete(int.Parse(lbxDatos.SelectedValue.ToString()));
                }

                MessageBox.Show("Proveedor Eliminado Exitosamente!: ID del Proveedor ->" + txtGet.Text);

            }
            catch (Exception err)
            {
                MessageBox.Show("Error al eliminar a la Proveedor : " + err);
                throw err;
            }
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lbxDatos.SelectedValue != null)
            {
                Proveedor proveedor = ProveedorBrl.Get(int.Parse(lbxDatos.SelectedValue.ToString()));
                ProveedorInsert personaUpdateVentana = new ProveedorInsert(proveedor);
                personaUpdateVentana.Owner = this;
                personaUpdateVentana.ShowDialog();
            }
        }
    }
}
