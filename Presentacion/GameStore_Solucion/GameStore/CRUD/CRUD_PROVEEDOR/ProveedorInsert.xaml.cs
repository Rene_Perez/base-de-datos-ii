﻿using GameStore.Common;
using ProveedorBRL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameStore.CRUD.CRUD_PROVEEDOR
{
    /// <summary>
    /// Lógica de interacción para ProveedorInsert.xaml
    /// </summary>
    public partial class ProveedorInsert : Window
    {
        public ProveedorInsert()
        {
            InitializeComponent();
            btnAgregar.Visibility = Visibility.Visible;
            btnActualizar.Visibility = Visibility.Hidden;
        }

        public ProveedorInsert(Proveedor proveedor)
        {
            InitializeComponent();
            txtId.Text = proveedor.IdProveedor.ToString();
            txtRazonSocial.Text = proveedor.RazonSocial;
            txtTelefono.Text = proveedor.Telefono;
            
            btnAgregar.Visibility = Visibility.Hidden;
            btnActualizar.Visibility = Visibility.Visible;
        }
               

        private void BtnAgregar_Click(object sender, RoutedEventArgs e)
        {
            Proveedor Proveedor = new Proveedor()
            {
                RazonSocial = txtRazonSocial.Text.Trim(),
                Telefono = txtTelefono.Text.Trim()
            };
            try
            {
                ProveedorBrl.Insert(Proveedor);
                MessageBox.Show("Proveedor Insertado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al insertar el Proveedor" + err);
                throw err;
            }
        }

        private void BtnActualizar_Click_1(object sender, RoutedEventArgs e)
        {
            Proveedor proveedor = new Proveedor()
            {
                IdProveedor = int.Parse(txtId.Text),
                RazonSocial = txtRazonSocial.Text.Trim(),
                Telefono = txtTelefono.Text.Trim()
            };
            try
            {
                ProveedorBrl.Update(proveedor);
                MessageBox.Show("Proveedor Actualizado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al actualizar la Proveedor" + err);
                throw err;
            }
        }
    }
}
