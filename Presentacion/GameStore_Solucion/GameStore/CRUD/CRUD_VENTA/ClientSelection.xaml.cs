﻿using GameStore.BRL_Persona;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameStore.CRUD.CRUD_VENTA
{
    /// <summary>
    /// Lógica de interacción para ClientSelection.xaml
    /// </summary>
    public partial class ClientSelection : Window, InterfazVenta
    {
        public ClientSelection()
        {
            InitializeComponent();
        }

        public Guid obtenerId(Guid idCliente)
        {
            throw new NotImplementedException();
        }

        public void RefrescarClientes()
        {
            dgDatos.ItemsSource = ClienteKeyValueListBrl.Get(txtGet.Text.ToString());
            dgDatos.DisplayMemberPath = "Nombre";
            dgDatos.SelectedValuePath = "IdCliente";
        }

        private void BtnSeleccionar_Click(object sender, RoutedEventArgs e)
        {
            InterfazVenta i = this.Owner as InterfazVenta;
            if (i != null)
            {
                i.obtenerId(Guid.Parse(dgDatos.SelectedValue.ToString()));
            }
            Close();
        }

        private void BtnAgregarCliente_Click(object sender, RoutedEventArgs e)
        {
            CRUD_CLIENTE.ClienteInsert ventana = new CRUD_CLIENTE.ClienteInsert();
            ventana.Owner = this;
            ventana.Show();
        }

        private void TxtGet_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarClientes();
        }
    }
}
