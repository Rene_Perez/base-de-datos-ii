﻿using System;
using System.Windows;
using GameStore.Common;
using GameStore.BRL_Persona;
using System.Data;

namespace GameStore.CRUD.CRUD_EMPLEADO
{
    /// <summary>
    /// Lógica de interacción para EmpleadoCrud.xaml
    /// </summary>
    public partial class EmpleadoCrud : Window
    {
        public EmpleadoCrud()
        {
            InitializeComponent();
        }
        public void RefrescarEmpleados()
        {
            dtgDatos.ItemsSource = EmpleadoKeyValueListBrl.Get(txtGet.Text.ToString());
            dtgDatos.DisplayMemberPath = "nombreCompleto";
            dtgDatos.SelectedValuePath = "IdPersona";
        }
        private void BtnInsert_Click(object sender, RoutedEventArgs e)
        {
            EmpleadoInsert empleadoInsertVentana = new EmpleadoInsert();
            empleadoInsertVentana.Show();
        }

        private void TxtGet_TextChanged(object sender, System.Windows.Controls.TextChangedEventArgs e)
        {
            RefrescarEmpleados();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (dtgDatos.SelectedValue!=null)
            {
                Empleado Empleado = EmpleadoBrl.Obtener(Guid.Parse(dtgDatos.SelectedValue.ToString()));
                EmpleadoInsert ventana = new EmpleadoInsert();
                ventana.Owner = this;
                ventana.Show();
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (dtgDatos.SelectedValue != null)
                {
                    EmpleadoBrl.Eliminar(Guid.Parse(dtgDatos.SelectedValue.ToString()));
                }

                MessageBox.Show("Empleado Eliminado Exitosamente!: ID del Empleado ->" + txtGet.Text);

            }
            catch (Exception err)
            {
                MessageBox.Show("Error al eliminar a la Empleado : " + err);
                throw err;
            }
        }
    }
}
