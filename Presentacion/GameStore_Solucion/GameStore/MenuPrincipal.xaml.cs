﻿using GameStore.Common;
using System.Windows;
using System.Windows.Controls;

namespace GameStore
{
    /// <summary>
    /// Lógica de interacción para MenuPrincipal.xaml
    /// </summary>
    public partial class MenuPrincipal : Window
    {


        public MenuPrincipal()
        {
            InitializeComponent();
        }

        public MenuPrincipal(Usuario usuarioSession)
        {
            InitializeComponent();

            tbkUsuario.Text = usuarioSession.NombreUsuario;
            tbkRol.Text = usuarioSession.Rol;



            if (usuarioSession.Rol == "empleado")
            {
                itemHome.Visibility = Visibility.Collapsed;
                itemEmpleado.Visibility = Visibility.Collapsed;
            }
            else if (usuarioSession.Rol == "administrador")
            {
                itemHome.Visibility = Visibility.Visible;
                itemCompra.Visibility = Visibility.Visible;
                itemVenta.Visibility = Visibility.Visible;
                itemEmpleado.Visibility = Visibility.Visible;
            }
            else
            {
                itemHome.Visibility = Visibility.Collapsed;
                itemCompra.Visibility = Visibility.Collapsed;
                itemVenta.Visibility = Visibility.Collapsed;
                itemEmpleado.Visibility = Visibility.Collapsed;
            }
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void btnCollapseMenu_Click(object sender, RoutedEventArgs e)
        {
            btnCollapseMenu.Visibility = Visibility.Collapsed;
            btnOpenMenu.Visibility = Visibility.Visible;
            imgLogo.Visibility = Visibility.Collapsed;
        }

        private void btnOpenMenu_Click(object sender, RoutedEventArgs e)
        {
            btnCollapseMenu.Visibility = Visibility.Visible;
            btnOpenMenu.Visibility = Visibility.Collapsed;
            imgLogo.Visibility = Visibility.Visible;
        }

        private void lvwMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            switch (((ListViewItem)((ListView)sender).SelectedItem).Name)
            {
                //case "itemHome":
                //    usc = new UserControlHome();

                //    break;
                case "itemHome":
                    menuPruebas menuPruebas = new menuPruebas();
                    menuPruebas.Show();
                    break;

                case "itemCompra":
                    /*aqui abre la ventana de compras*/
                    CRUD.COMPRA_PRODUCTO.ProductoCompra productoCompra = new CRUD.COMPRA_PRODUCTO.ProductoCompra();
                    productoCompra.Owner = this;
                    productoCompra.Show();
                    break;
                case "itemVenta":
                    /*aqui abre la ventana de ventas*/
                    CRUD.CRUD_VENTA.VentaInsert ventaInsert = new CRUD.CRUD_VENTA.VentaInsert();
                    ventaInsert.Owner = this;
                    ventaInsert.Show();
                    break;
                case "itemEmpleado":
                    CRUD.CRUD_EMPLEADO.EmpleadoCrud empleadoCrud = new CRUD.CRUD_EMPLEADO.EmpleadoCrud();
                    empleadoCrud.Owner = this;
                    empleadoCrud.Show();
                    break;
                case "itemProveedor":
                    CRUD.CRUD_PROVEEDOR.ProveedorCrud proveedorCrud = new CRUD.CRUD_PROVEEDOR.ProveedorCrud();
                    proveedorCrud.Owner = this;
                    proveedorCrud.Show();
                    break;
                case "itemProducto":
                    CRUD.CRUD_PRODUCTO.ProductoCrud productoCrud = new CRUD.CRUD_PRODUCTO.ProductoCrud();
                    productoCrud.Owner = this;
                    productoCrud.Show();
                    break;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnCambiar_Click(object sender, RoutedEventArgs e)
        {
            login login = new login();
            login.Show();
            this.Close();


        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show(Sesion.idSesion + "");
        }

    }
}
