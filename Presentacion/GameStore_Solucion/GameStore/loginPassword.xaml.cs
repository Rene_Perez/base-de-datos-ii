﻿using GameStore.BRL_Persona;
using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameStore
{
    /// <summary>
    /// Lógica de interacción para loginPassword.xaml
    /// </summary>
    public partial class loginPassword : Window
    {
        /// <summary>
        /// Auxiliar que se utiliza para obtener el id de Sesion
        /// </summary>

        /// <summary>
        /// Objeto User que se enviara para cambiar la contraseña si esque el estadoPassword = 0
        /// </summary>
        Usuario user = new Usuario();
        public loginPassword()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Cargamos el ID
        /// </summary>
        /// <param name="id"></param>
        public loginPassword(Usuario usuario)
        {
            InitializeComponent();
            txtIdUsuario.Text = usuario.IdUsuario.ToString();
            txtPassword.Password = usuario.Password.ToString();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();

        }

        private void btnCambiarContraseña_Click(object sender, RoutedEventArgs e)
        {
            if (txtPassword.Password == txtConfirmarPassword.Password)
            {
                Usuario usuario = new Usuario()
                {
                    IdUsuario =  Guid.Parse(txtIdUsuario.Text.Trim()),
                    Password = txtPassword.Password
                };
                UsuarioBrl.CambiarPassword(usuario);
                MessageBox.Show("Password Actualizado Correctamente");
                this.Close();
            }
            else
            {
                MessageBox.Show("Ambos campos deben coincidir");
            }
        }



        private void txtConfirmarPassword_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void txtPassword_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            else
                e.Handled = false;
        }
    }
}

