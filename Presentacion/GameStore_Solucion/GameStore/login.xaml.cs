﻿using GameStore.BRL_Persona;
using GameStore.Common;
using System;
using System.Data;
using System.Windows;
using System.Windows.Input;

namespace GameStore
{
    /// <summary>
    /// Lógica de interacción para login.xaml
    /// </summary>
    public partial class login : Window
    {
      
        /// <summary>
        /// Variable de objeto UsuarioBRL el cual es la clase logica que realiza las invocaciones a los metodos de interaccion con la base de datos para realizar el logeo
        /// </summary>
        byte cont = 0;

        public login()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnSalir_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnIngresar_Click(object sender, RoutedEventArgs e)
        {
            if (txtNombreUsuario.Text != "" && txtPassword.Password != "")
            {
                try
                {

                    if (!DAL_Persona.Login.ExisteUsuario(txtNombreUsuario.Text, txtPassword.Password))
                    {
                        tbkDetalle.Text = "Intente De Nuevo :)";
                        cont++;
                        txtNombreUsuario.Clear();
                        txtPassword.Clear();
                        txtNombreUsuario.Focus();
                        if (cont > 3)
                        {
                            MessageBox.Show("Demasiado intentos");
                            this.Close();
                        }
                        return;
                    }
                    else
                    {
                        Usuario usuario = UsuarioBrl.ObtenerIdUsuario(txtNombreUsuario.Text);
                        if (usuario.EstadoPasswrod == 0)
                        {
                            MessageBox.Show("Es necesario Cambiar password");
                            loginPassword loginPassword = new loginPassword(usuario);
                            loginPassword.ShowDialog();
                        }
                        else if (usuario.EstadoPasswrod == 1)
                        {
                            Usuario usuarioSession = UsuarioBrl.ObtenerSession(txtNombreUsuario.Text, txtPassword.Password);


                            MenuPrincipal menuPrincipal = new MenuPrincipal(usuarioSession);
                            menuPrincipal.ShowDialog();
                        }
                        
                    }
                }
                catch (Exception ex)
                {

                    throw ex;
                }
            }
            else
            {
                tbkDetalle.Text = "Es necesario llenar los campos";
                cont++;
                if (cont > 3)
                {
                    MessageBox.Show("Demasiado intentos");
                    this.Close();
                }
            }
        }

        private void txtNombreUsuario_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void txtPassword_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
            {
                e.Handled = true;
            }
            else
                e.Handled = false;
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //txtNombreUsuario.Focus();
            txtNombreUsuario.Text = "admin";
            txtPassword.Password = "admin";
        }


    }
}
