﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;

namespace VentaDAL
{
    public class VentaDal
    {
        public static void Insert(Venta venta)
        {
            Operaciones.WriteLogsDebug("VentaDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear una Venta"));

            SqlCommand command = null;

            string query = @"INSERT INTO Venta(idVenta,totalVenta,fecha,idEmpleado,idCliente)
                            VALUES(@idVenta,@totalVenta,@fecha,@idEmpleado,@idCliente)";

            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                conexion.Open();

                transaccion = conexion.BeginTransaction();
                command = OperacionesSql.CreateBasicCommand(query);
                command.Parameters.AddWithValue("@idVenta", venta.IdVenta);
                command.Parameters.AddWithValue("@totalVenta", venta.TotalVenta);
                command.Parameters.AddWithValue("@fecha", venta.Fecha);
                command.Parameters.AddWithValue("@idEmpleado", venta.IdEmpleado);
                command.Parameters.AddWithValue("@idCliente", venta.IdCliente);
                OperacionesSql.ExecuteBasicCommand(command);                

                foreach (DetalleVenta detalle in venta.DetalleVenta)
                {                    
                    DetalleVentaDal.InsertTransaction(detalle, transaccion, conexion);
                    
                }

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

            Operaciones.WriteLogsDebug("VentaDal", "Insertar", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar venta"));
        }

        /*public static void InsertTransaction(Venta venta, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("VentaDal", "InsertarTransaccion", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un Venta"));

            SqlCommand command = null;

            //Consulta para insertar Ventas
            string query = @"INSERT INTO Venta(totalVenta,fecha,idEmpleado,idCliente)
                            VALUES(@totalVenta,@fecha,@idEmpleado,@idCliente)";
            try
            {
                command = OperacionesSql.CreateBasicCommand(query);
                command.Parameters.AddWithValue("@totalVenta", venta.TotalVenta);
                command.Parameters.AddWithValue("@fecha", venta.Fecha);
                command.Parameters.AddWithValue("@idEmpleado", venta.IdEmpleado);
                command.Parameters.AddWithValue("@idCliente", venta.IdCliente);
                OperacionesSql.ExecuteBasicCommandWithTransaction(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "InsertarTransaccion", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "InsertarTransaccion", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("VentaDal", "InsertarTransaccion", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar Venta"));
        }
        */
        /*
        public static Venta Get(int id)
        {
            Venta res = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT V.totalVenta,fecha,idEmpleado,idCliente 
                             FROM Venta V 
                             INNER JOIN
                             WHERE idVenta=@id and estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Venta()
                    {
                        IdVenta = id,
                        //TotalVenta = SqlDataType(0),  MONEY
                        Fecha = dr.GetDateTime(1),
                        IdEmpleado = dr.GetInt32(2),
                        IdCliente = dr.GetInt32(3)
                    };
                    detalleVenta.CantidadProducto = dr.GetDateTime(1);
                    detalleVenta.PrecioUnitario = dr.GetBoolean(2);
                    detalleVenta.Detalle = dr.GetBoolean(2);
                    detalleVenta.FechaVenta = dr.GetBoolean(2);
                    detalleVenta.IdVenta = dr.GetBoolean(2);
                    detalleVenta.IdProducto = dr.GetBoolean(2);
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        public static List<Venta> Select()
        {
            List<Venta> res = new List<Venta>();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idVenta,totalVenta,fecha,idEmpleado,idCliente FROM Venta WHERE estado=1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);                
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    Venta venta = new Venta()
                    {
                        IdVenta = dr.GetInt32(0),
                        //TotalVenta = SqlDataType(1),  MONEY
                        Fecha = dr.GetDateTime(2),
                        IdEmpleado = dr.GetInt32(3),
                        IdCliente = dr.GetInt32(4)
                        
                    };
                    res.Add(venta);
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }

    */
        public static void Eliminar(int id)
        {
            Operaciones.WriteLogsDebug("VentaDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Venta"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Venta SET estado=0
                                    WHERE idVenta = @idVenta";
            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idVenta", id);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("VentaDal", "Eliminar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }
    }
}
