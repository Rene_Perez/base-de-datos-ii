﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;


namespace VentaDAL
{
    public class DetalleVentaDal
    {
        public static void InsertTransaction(DetalleVenta detalle, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("DetalleVentaDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un DetalleVenta"));

            SqlCommand command = null;

            //Consulta para insertar DetalleVentas
            string queryString = @"INSERT INTO DetalleVenta(idVenta,cantidadProducto,precioUnitario,idProducto) 
                                    VALUES(@idVenta,@cantidadProducto,@precioUnitario,@idProducto)";
            
            try
            {                
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);

                command.Parameters.AddWithValue("@idVenta", detalle.IdVenta);
                command.Parameters.AddWithValue("@cantidadProducto", detalle.CantidadProducto);
                command.Parameters.AddWithValue("@precioUnitario", detalle.PrecioUnitario);
                command.Parameters.AddWithValue("@idProducto", detalle.IdProducto);

                   
                UpdateStock(detalle.IdProducto,detalle.CantidadProducto,transaccion,conexion);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("DetalleVentaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("DetalleVentaDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            
            Operaciones.WriteLogsDebug("DetalleVentaDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar DetalleVenta"));
        }

        /// <summary>
        /// Actualiza Usuario de la base de datos
        /// </summary>
        /// <param name="Producto"></param>
        public static void UpdateStock(int id, int cantidad, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Producto"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Producto SET stock=stock-@cantidad
                             WHERE idProducto=@idProducto";
            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idProducto", id);
                command.Parameters.AddWithValue("@cantidad", cantidad);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }


        /*
        public static DetalleVenta Obtener(int id)
        {
            DetalleVenta detalleVenta = null;
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT IdPersona,FechaContratacion, Eliminado 
                             FROM Venta V
                             INNER JOIN 
                             WHERE Venta=@id";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                detalleVenta.Venta = VentaDal.Get(id);
                while (dr.Read())
                {
                    
                }

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Obtenet", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("VentaDal", "Obtenet", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return Venta;
        }
        */
    }
}
