﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompraDAL
{
    public class CompraDal
    {
        public static void Insert(Compra Compra)
        {
            Operaciones.WriteLogsDebug("CompraDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear una Compra"));

            SqlCommand command = null;

            string query = @"INSERT INTO Compra(idCompra,totalCompra,fecha,idEmpleado,idProveedor)
                            VALUES(@idCompra,@totalCompra,@fecha,@idEmpleado,@idProveedor)";

            //Declaro e inicio la conexion
            SqlConnection conexion = OperacionesSql.ObtenerConexion();

            //Declaro la transaccion
            SqlTransaction transaccion = null;
            try
            {
                conexion.Open();

                transaccion = conexion.BeginTransaction();
                command = OperacionesSql.CreateBasicCommand(query);
                command.Parameters.AddWithValue("@idCompra", Compra.IdCompra);
                command.Parameters.AddWithValue("@totalCompra", Compra.TotalCompra);
                command.Parameters.AddWithValue("@fecha", Compra.Fecha);
                command.Parameters.AddWithValue("@idEmpleado", Compra.IdEmpleado);
                command.Parameters.AddWithValue("@idProveedor", Compra.IdProveedor);
                OperacionesSql.ExecuteBasicCommand(command);

                foreach (DetalleCompra detalle in Compra.DetalleCompra)
                {
                    DetalleCompraDal.InsertTransaction(detalle, transaccion, conexion);

                }

                transaccion.Commit();
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("CompraDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("CompraDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                conexion.Close();
            }

            Operaciones.WriteLogsDebug("CompraDal", "Insertar", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar Compra"));
        }
    }
}
