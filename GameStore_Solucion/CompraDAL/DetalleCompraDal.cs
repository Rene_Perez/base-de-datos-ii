﻿
using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CompraDAL
{
    public class DetalleCompraDal
    {
        public static void InsertTransaction(DetalleCompra detalle, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("DetalleCompraDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un DetalleCompra"));

            SqlCommand command = null;

            //Consulta para insertar DetalleCompras
            string queryString = @"INSERT INTO DetalleCompra(idCompra,cantidadProducto,precioUnitario,idProducto) 
                                    VALUES(@idCompra,@cantidadProducto,@precioUnitario,@idProducto)";

            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);

                command.Parameters.AddWithValue("@idCompra", detalle.IdCompra);
                command.Parameters.AddWithValue("@cantidadProducto", detalle.CantidadProducto);
                command.Parameters.AddWithValue("@precioUnitario", detalle.PrecioUnitario);
                command.Parameters.AddWithValue("@idProducto", detalle.IdProducto);


                UpdateStock(detalle.IdProducto, detalle.CantidadProducto, transaccion, conexion);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("DetalleCompraDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("DetalleCompraDal", "Insertar", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("DetalleCompraDal", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar DetalleCompra"));
        }

        /// <summary>
        /// Actualiza Usuario de la base de datos
        /// </summary>
        /// <param name="Producto"></param>
        public static void UpdateStock(int id, int cantidad, SqlTransaction transaccion, SqlConnection conexion)
        {
            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Producto"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Producto SET stock=stock+@cantidad
                             WHERE idProducto=@idProducto";
            try
            {
                command = OperacionesSql.CreateBasicCommandWithTransaction(queryString, transaccion, conexion);
                command.Parameters.AddWithValue("@idProducto", id);
                command.Parameters.AddWithValue("@cantidad", cantidad);

                OperacionesSql.ExecuteBasicCommandWithTransaction(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }
    }
}
