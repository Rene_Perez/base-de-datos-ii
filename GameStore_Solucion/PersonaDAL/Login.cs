﻿using GameStore.DAL_Persona.DataSetTableAdapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.DAL_Persona
{
    public class Login
    {
        private static UsuarioTableAdapter adaptador = new UsuarioTableAdapter();

        public static bool ExisteUsuario(string nombreUsuario, string password)
        {
            if (adaptador.ExisteUsuario(nombreUsuario, password) == null)
            {
                return false;
            }
            else
            {
                return true;
            }
            
        }
        public static bool EstadoPassword()
        {
            if (adaptador.EstadoPassword() == null)
            {
                return false;
            }
            else
            {
                return true;
            }

        }
    }
}
