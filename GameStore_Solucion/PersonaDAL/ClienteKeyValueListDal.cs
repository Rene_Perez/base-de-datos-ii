﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;

namespace GameStore.DAL_Persona
{
    public class ClienteKeyValueListDal
    {
        /// <summary>
        /// Retorna una lista de identifificadores y nombre completo de Productos
        /// </summary>
        /// <param name="apellido">Apellido paterno de Productos</param>
        /// <returns></returns>
        public static ClienteKeyValueList Get(string nombre)
        {
            ClienteKeyValueList lista = new ClienteKeyValueList();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT p.idPersona,p.ci,CONCAT(p.nombres,' ', p.primerApellido,' ',p.segundoApellido) as 'nombreCompleto', p.fechaNacimiento,c.puntos
                            FROM Cliente c
                            INNER JOIN Persona p ON p.idPersona = c.idPersona
                            Where p.estado = 1 and (p.nombres like @nombreCompleto or p.primerApellido like @nombreCompleto)";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@nombreCompleto", string.Format("%{0}%", nombre));
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    lista.Add(new ClienteKeyValue()
                    {
                        IdPersona = dr.GetGuid(0),
                        Ci = dr.GetString(1),
                        NombreCompleto = dr.GetString(2),
                        FechaNacimiento = dr.GetDateTime(3),
                        Puntos = dr.GetInt16(4),
                        Estado = 1
                    });
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteDal", "Obtenet(Get)", string.Format("{0} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return lista;
        }
    }
}
