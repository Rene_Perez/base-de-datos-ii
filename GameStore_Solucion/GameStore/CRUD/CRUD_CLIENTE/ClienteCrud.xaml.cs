﻿using System;
using System.Windows;
using System.Windows.Controls;
using GameStore.Common;
using GameStore.BRL_Persona;
using System.Data;

namespace GameStore.CRUD.CRUD_CLIENTE
{
    /// <summary>
    /// Lógica de interacción para ClienteCrud.xaml
    /// </summary>
    public partial class ClienteCrud : Window
    {
        public ClienteCrud()
        {
            InitializeComponent();
        }

        public void RefrescarClientes()
        {
            lbxDatos.ItemsSource = ClienteKeyValueListBrl.Get(txtGet.Text.ToString());
            lbxDatos.DisplayMemberPath = "NombreCompleto";
            lbxDatos.SelectedValuePath = "IdPersona";
        }

        
        /*
        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lbxDatos.SelectedValue != null)
            {
                Cliente Cliente = ClienteBrl.Get(Guid.Parse(lbxDatos.SelectedValue.ToString()));
                ClienteInsert personaUpdateVentana = new ClienteInsert(Cliente);
                personaUpdateVentana.ShowDialog();
            }

        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ClienteBrl.SoftDelete(Guid.Parse(lbxDatos.SelectedValue.ToString()));
                MessageBox.Show("Cliente Eliminado Exitosamente!: ID del Cliente ->" + txtGet.Text);               
                    
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al eliminar a la Cliente : " + err);
                throw err;
            }
        }
        */
        private void TxtGet_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarClientes();
        }

        private void BtnInsert_Click_1(object sender, RoutedEventArgs e)
        {
            ClienteInsert ClienteInsertVentana = new ClienteInsert();
            ClienteInsertVentana.Owner = this;
            ClienteInsertVentana.ShowDialog();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lbxDatos.SelectedValue != null)
            {
                Cliente cliente = ClienteBrl.Get(Guid.Parse(lbxDatos.SelectedValue.ToString()));
                ClienteInsert personaUpdateVentana = new ClienteInsert(cliente);
                personaUpdateVentana.ShowDialog();
            }
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (lbxDatos.SelectedValue != null)
                {
                    ClienteBrl.SoftDelete(Guid.Parse(lbxDatos.SelectedValue.ToString()));
                }
                
                MessageBox.Show("Cliente Eliminado Exitosamente!: ID del Cliente ->" + txtGet.Text);

            }
            catch (Exception err)
            {
                MessageBox.Show("Error al eliminar a la Cliente : " + err);
                throw err;
            }
        }
    }
}
