﻿using System;
using System.Windows;
using System.Windows.Controls;
using GameStore.BRL_Persona;
using GameStore.Common;

namespace GameStore.CRUD.CRUD_CLIENTE
{
    /// <summary>
    /// Lógica de interacción para ClienteInsert.xaml
    /// </summary>
    public partial class ClienteInsert : Window
    {
        public ClienteInsert()
        {
            InitializeComponent();
            btnActualizar.Visibility = Visibility.Hidden;
            btnInsertar.Visibility = Visibility.Visible;
        }

        public ClienteInsert(Cliente cliente)
        {
            InitializeComponent();
            btnActualizar.Visibility = Visibility.Visible;            
            txtCarnetIdentidad.Text = cliente.Ci.ToString();
            txtNombres.Text = cliente.Nombres.ToString();
            txtPrimerApellido.Text = cliente.PrimerApellido.ToString();
            txtSegundoApellido.Text = cliente.SegundoApellido.ToString();            
            dpFechaNacimiento.Text = cliente.FechaNacimiento.ToString();
            txtDireccion.Text = cliente.Direccion.ToString();
            txtPuntos.Text = cliente.Puntos.ToString();
            btnInsertar.Visibility = Visibility.Hidden;
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Cliente cliente = new Cliente()
            {
                IdPersona = Guid.NewGuid(),
                Ci = txtCarnetIdentidad.Text.Trim(),
                Nombres = txtNombres.Text.Trim(),
                PrimerApellido = txtPrimerApellido.Text.Trim(),
                SegundoApellido = txtSegundoApellido.Text.Trim(),
                FechaNacimiento = DateTime.Parse(dpFechaNacimiento.SelectedDate.Value.ToShortDateString()),
                Sexo = getSexo(),
                Direccion = txtDireccion.Text.Trim(),
                Puntos = short.Parse(txtPuntos.Text.Trim())
            };

            try
            {
                ClienteBrl.Insert(cliente);
                MessageBox.Show("Cliente Insertado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al insertar el cliente" + err);
                throw err;
            }
        }

        private void BtnActualizar_Click(object sender, RoutedEventArgs e)
        {            
            Cliente cliente = new Cliente()
            {
                IdPersona = Guid.Parse(txtId.Text.Trim()),
                Ci = txtCarnetIdentidad.Text.Trim(),
                Nombres = txtNombres.Text.Trim(),
                PrimerApellido = txtPrimerApellido.Text.Trim(),
                SegundoApellido = txtSegundoApellido.Text.Trim(),
                FechaNacimiento = DateTime.Parse(dpFechaNacimiento.SelectedDate.Value.ToShortDateString()),
                Sexo = getSexo(),
                Direccion = txtDireccion.Text.Trim(),
                Puntos = short.Parse(txtPuntos.Text.Trim())

            };
            try
            {
                ClienteBrl.Update(cliente);
                MessageBox.Show("Cliente Actualizado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al actualizar la persona" + err);
                throw err;
            }
        }

        private byte getSexo()
        {
            if (rbtSexoMasculino.IsChecked == true)
            {
                return 1;
            }
            else
            {
                return 2;
            }
            
        }

        
    }
}
