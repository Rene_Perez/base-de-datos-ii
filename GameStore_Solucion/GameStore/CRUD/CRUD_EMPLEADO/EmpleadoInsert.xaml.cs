﻿using System;
using System.Windows;
using GameStore.Common;
using GameStore.BRL_Persona;
using System.Data.SqlTypes;

namespace GameStore.CRUD.CRUD_EMPLEADO
{
    /// <summary>
    /// Lógica de interacción para EmpleadoInsert.xaml
    /// </summary>
    public partial class EmpleadoInsert : Window
    {
        public EmpleadoInsert()
        {
            InitializeComponent();
            DataContext = this;
        }

        private void BtnInsertarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            Operaciones.WriteLogsDebug("InsertarEmpleado", "btnCrear_Click", string.Format("{0} Info: {1}",
           DateTime.Now.ToString(),
           "Empezando a ejecutar el metodo de la capa de presentacion para crear un empleado"));

            try
            {

                Empleado empleado = new Empleado();
                empleado.IdPersona = Guid.NewGuid();

                empleado.Ci = txtCarnetIdentidad.Text.Trim();
                empleado.Nombres = txtNombres.Text.Trim();
                empleado.PrimerApellido = txtPrimerApellido.Text.Trim();
                empleado.SegundoApellido = txtSegundoApellido.Text.Trim();
                empleado.Sexo = 1;
                empleado.FechaNacimiento = dpFechaNacimiento.SelectedDate.Value;
                empleado.Direccion = txtDireccion.Text.Trim();

                empleado.Sueldo = SqlMoney.Parse(txtSueldo.Text);
                empleado.Cargo = 1;

                empleado.Usuario = new Usuario();
                empleado.Usuario.IdUsuario = Guid.NewGuid();
                empleado.Usuario.NombreUsuario = txtNombreUsuario.Text.Trim();
                empleado.Usuario.Password = txtPasswordUsuario.Text.Trim();

                EmpleadoBrl.Insertar(empleado);
                //NavegadorEmpleado navegador = this.Owner as NavegadorEmpleado;
                //navegador.RefrescarEmpleados();
                MessageBox.Show("Empleado Agregado Exitosamente");
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show("error" + ex);
                Operaciones.WriteLogsRelease("InsertarEmpleado", "btnCrear_Click", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                MessageBoxResult result = MessageBox.Show("Existe un problema, por favor contactese con su administrador",
                                          "Confirmation",
                                          MessageBoxButton.OK,
                                          MessageBoxImage.Error);
                if (result == MessageBoxResult.OK)
                {
                    Application.Current.Shutdown();
                }
            }

            Operaciones.WriteLogsDebug("InsertarEmpleado", "btnCrear_Click", string.Format("{0} Info: {1}",
              DateTime.Now.ToString(),
              "Termino a ejecutar el metodo de la capa de presentacion para crear un empleado"));

        }
        private byte getSexo()
        {
            if (rbtSexoMasculino.IsChecked==true)
            {
                return 1;
            }
            else if (rbtSexoFemenino.IsChecked==true)
            {
                return 2;
            }
            return 0;
        }
    }
}
