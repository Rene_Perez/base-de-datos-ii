﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using GameStore.BRL_Producto;
using GameStore.Common;
using VentaBRL;

namespace GameStore.CRUD.CRUD_VENTA
{
    /// <summary>
    /// Lógica de interacción para VentaInsert.xaml
    /// </summary>
    public partial class VentaInsert : Window
    {
        List<DetalleVenta> detalles = new List<DetalleVenta>();
        Guid idVenta;
        Producto producto;
        public VentaInsert()
        {
            InitializeComponent();
            idVenta = Guid.NewGuid();
        }

        public void RefrescarProductos()
        {
            dgProductos.ItemsSource = ProductoKeyValueListBrl.Get(txtGetProductos.Text.ToString());
            dgProductos.DisplayMemberPath = "Nombre";
            dgProductos.SelectedValuePath = "IdProducto";
        }

        public void RefrescarDetalles()
        {
            dgLista.ItemsSource = null;
            dgLista.ItemsSource = detalles;
        }

        private void BtnAgregarProducto_Click(object sender, RoutedEventArgs e)
        {
            producto = ProductoBrl.Get(int.Parse(dgProductos.SelectedValue.ToString()));
            detalles.Add(new DetalleVenta()
            {
                IdProducto = producto.IdProducto,
                CantidadProducto = int.Parse(txtCantidad.Text),
                PrecioUnitario = producto.Precio * int.Parse(txtCantidad.Text),
                IdVenta = idVenta
            });

            RefrescarDetalles();
        }

        private void BtnRealizarVenta_Click(object sender, RoutedEventArgs e)
        {
            SqlMoney total = 0;
            foreach (DetalleVenta detalle in detalles)
            {
                total += detalle.PrecioUnitario;
            }
            Venta venta = new Venta()
            {
                DetalleVenta = detalles,
                TotalVenta = total,
                IdEmpleado = Guid.Parse(txtEmpleado.Text),
                Fecha = DateTime.Parse(DateTime.Now.ToShortDateString()),
                IdVenta = idVenta,
                IdCliente = Guid.Parse(txtCliente.Text)
            };
            MessageBox.Show("Precio Total: " + venta.TotalVenta + "\nFecha: " + venta.Fecha);
            try
            {
                MessageBox.Show("Venta realizada exitosamente", "Advertencia", MessageBoxButton.OKCancel);
                VentaBrl.Insert(venta);
                detalles = new List<DetalleVenta>();
                dgLista.ItemsSource = null;
            }
            catch (Exception)
            {

                throw;
            }
            RefrescarProductos();
        }

        private void TxtGetProductos_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarProductos();
        }

        private void BtnAgregarCliente_Click(object sender, RoutedEventArgs e)
        {
            ClientSelection ventana = new ClientSelection();
            ventana.Owner = this;
            ventana.Show();
        }

        private void BtnAgregarEmpleado_Click(object sender, RoutedEventArgs e)
        {
            CRUD_EMPLEADO.EmpleadoCrud ventana = new CRUD_EMPLEADO.EmpleadoCrud();
            ventana.Owner = this;
            ventana.Show();
        }
    }
}
