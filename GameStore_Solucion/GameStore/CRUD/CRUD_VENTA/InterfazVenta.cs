﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.CRUD.CRUD_VENTA
{
    public interface InterfazVenta
    {
        Guid obtenerId(Guid idCliente);
    }
}
