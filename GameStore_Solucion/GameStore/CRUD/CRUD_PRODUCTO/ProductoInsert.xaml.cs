﻿using System;
using System.Windows;
using GameStore.Common;
using GameStore.BRL_Producto;
using System.Data.SqlTypes;

namespace GameStore.CRUD.CRUD_PRODUCTO
{
    /// <summary>
    /// Lógica de interacción para ProductoInsert.xaml
    /// </summary>
    public partial class ProductoInsert : Window
    {
        public ProductoInsert()
        {
            InitializeComponent();
            btnInsertar.Visibility = Visibility.Visible;
            btnActualizar.Visibility = Visibility.Hidden;
        }

        public ProductoInsert(Producto producto)
        {
            InitializeComponent();
            txtId.Text = producto.IdProducto.ToString();
            txtImagen.Text = producto.Imagen.ToString();            
            txtNombre.Text = producto.Nombre.ToString();
            txtDescripcion.Text = producto.Descripcion.ToString();
            //this.rbtSexoOtro.IsChecked = cliente.Sexo.ToString());
            txtPrecio.Text = producto.Precio.ToString();
            txtStock.Text = producto.Stock.ToString();
            btnInsertar.Visibility = Visibility.Hidden;
            btnActualizar.Visibility = Visibility.Visible;
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            Producto producto = new Producto()
            {
                Nombre = txtNombre.Text,
                Imagen = "Default",                
                Descripcion = txtDescripcion.Text,
                Precio = SqlMoney.Parse(txtPrecio.Text),
                Stock = 20,
                Tipo = 1
                //Stock = short.Parse(txtStock.Text)
            };
            try
            {
                ProductoBrl.Insert(producto);
                MessageBox.Show("Producto Insertado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al insertar el Producto" + err);
                throw err;
            }
        }

        private void BtnActualizar_Click(object sender, RoutedEventArgs e)
        {
            Producto producto = new Producto()
            {
                IdProducto = int.Parse(txtId.Text),
                Nombre = txtNombre.Text,
                Imagen = "Default",                
                Descripcion = txtDescripcion.Text,
                Precio = SqlMoney.Parse(txtPrecio.Text)
            };
            try
            {
                ProductoBrl.Update(producto);
                MessageBox.Show("Producto Actualizado Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al actualizar la Producto" + err);
                throw err;
            }
        }
        private short getTipoProducto()
        {
            if (rbtConsola.IsChecked == true)
            {
                return 0;//Consola
            }
            else if (rbtVideoJuego.IsChecked == true)
            {
                return 1;//Videojuego
            }
            {
                return 2;//Otro
            }
        }
    }
}
