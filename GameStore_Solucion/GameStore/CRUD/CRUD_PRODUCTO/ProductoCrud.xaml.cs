﻿using System;
using System.Windows;
using System.Windows.Controls;
using GameStore.Common;
using GameStore.BRL_Producto;
using System.Data;

namespace GameStore.CRUD.CRUD_PRODUCTO
{
    /// <summary>
    /// Lógica de interacción para ProductoCrud.xaml
    /// </summary>
    public partial class ProductoCrud : Window
    {
        public ProductoCrud()
        {
            InitializeComponent();
        }      

        public void RefrescarProductos()
        {
            lbxDatos.ItemsSource = ProductoKeyValueListBrl.Get(txtGet.Text.ToString());
            lbxDatos.DisplayMemberPath = "Nombre";
            lbxDatos.SelectedValuePath = "IdProducto";
        }
        
        private void BtnInsert_Click(object sender, RoutedEventArgs e)
        {
            ProductoInsert productoInsertVentana = new ProductoInsert();
            productoInsertVentana.Owner = this;
            productoInsertVentana.ShowDialog();
        }

        private void BtnUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (lbxDatos.SelectedValue != null)
            {
                Producto producto = ProductoBrl.Get(int.Parse(lbxDatos.SelectedValue.ToString()));
                ProductoInsert personaUpdateVentana = new ProductoInsert(producto);
                personaUpdateVentana.ShowDialog();
            }
            
        }

        private void BtnDelete_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                ProductoBrl.SoftDelete(int.Parse(lbxDatos.SelectedValue.ToString()));
                MessageBox.Show("Producto Eliminado Exitosamente!: ID del Producto ->" + txtGet.Text);
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al eliminar a la Producto : " + err);
                throw err;
            }
        }

        private void TxtGet_TextChanged(object sender, TextChangedEventArgs e)
        {
            RefrescarProductos();
        }
    }
}
