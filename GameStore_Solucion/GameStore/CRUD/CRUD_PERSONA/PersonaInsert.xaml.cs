﻿using System;
using System.Windows;
using GameStore.BRL_Persona;
using GameStore.Common;

namespace GameStore.CRUD.CRUD_PERSONA
{
    /// <summary>
    /// Lógica de interacción para PersonaInsert.xaml
    /// </summary>
    public partial class PersonaInsert : Window
    {   
        public PersonaInsert()
        {
            InitializeComponent();
        }

        public PersonaInsert(Persona persona)
        {
            InitializeComponent();
            this.txtIdPersona.Text = persona.IdPersona.ToString();
            this.txtCarnetIdentidad.Text = persona.Ci.ToString();
            this.txtNombres.Text = persona.Nombres.ToString();
            this.txtPrimerApellido.Text = persona.PrimerApellido.ToString();
            this.txtSegundoApellido.Text = persona.SegundoApellido.ToString();
            //this.rbtSexoOtro.IsChecked = persona.Sexo.ToString());
            this.dpFechaNacimiento.Text = persona.FechaNacimiento.ToString();
            this.txtDireccion.Text = persona.Direccion.ToString();
        }

        private void BtnInsertar_Click(object sender, RoutedEventArgs e)
        {
            //Persona persona = new Persona(txtCarnetIdentidad.Text, txtNombres.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, getSexo(), dpFechaNacimiento.SelectedDate.Value, txtDireccion.Text);
            try
            {
                //PersonaBrl.Insert(persona);
                MessageBox.Show("Persona Insertada Correctamente");
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al insertar la persona" + err);
                throw err;
            }
        }

        private void BtnActualizar_Click(object sender, RoutedEventArgs e)
        {
            //Persona persona = new Persona(int.Parse(txtIdPersona.Text),txtCarnetIdentidad.Text, txtNombres.Text, txtPrimerApellido.Text, txtSegundoApellido.Text, getSexo(), dpFechaNacimiento.SelectedDate.Value, txtDireccion.Text);
            try
            {
                //PersonaBrl.Update(persona);
                Close();
            }
            catch (Exception err)
            {
                MessageBox.Show("Error al actualizar la persona" + err);
                throw err;
            }         
        }

        private char getSexo()
        {
            if (rbtSexoMasculino.IsChecked == true)
            {
                return 'M';
            }
            else
            {
                return 'F';
            }
        }
    }
}
