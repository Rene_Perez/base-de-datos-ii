﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace GameStore
{
    /// <summary>
    /// Lógica de interacción para menuPruebas.xaml
    /// </summary>
    public partial class menuPruebas : Window
    {
        public menuPruebas()
        {
            InitializeComponent();
        }

        private void BtnPersonaCrud_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_PERSONA.PersonaCrud personaCrudVentana = new CRUD.CRUD_PERSONA.PersonaCrud();
            personaCrudVentana.Show();
        }

        private void BtnClienteCrud_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_CLIENTE.ClienteCrud clienteCrudVentana = new CRUD.CRUD_CLIENTE.ClienteCrud();
            clienteCrudVentana.Show();
        }

        private void BtnEmpleadoCrud_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_EMPLEADO.EmpleadoCrud empleadoCrudVentana = new CRUD.CRUD_EMPLEADO.EmpleadoCrud();
            empleadoCrudVentana.Show();
        }

        private void BtnComprarProducto_Click(object sender, RoutedEventArgs e)
        {
            CRUD.COMPRA_PRODUCTO.ProductoCompra productoCompraVentana = new CRUD.COMPRA_PRODUCTO.ProductoCompra();
            productoCompraVentana.Show();
        }

        private void BtnProductoCrud_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_PRODUCTO.ProductoCrud productoCrudVentana = new CRUD.CRUD_PRODUCTO.ProductoCrud();
            productoCrudVentana.Show();
        }

        private void BtnVenderProducto_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_VENTA.VentaInsert ventana = new CRUD.CRUD_VENTA.VentaInsert();
            ventana.Show();
        }

        private void BtnProveedorCrud_Click(object sender, RoutedEventArgs e)
        {
            CRUD.CRUD_PROVEEDOR.ProveedorCrud ventana = new CRUD.CRUD_PROVEEDOR.ProveedorCrud();
            ventana.Show();
        }
    }
}
