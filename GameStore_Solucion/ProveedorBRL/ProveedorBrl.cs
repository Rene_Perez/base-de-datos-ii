﻿using GameStore.Common;
using ProveedorDAL;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProveedorBRL
{
    public class ProveedorBrl
    {
        /// <summary>
        /// método lógica de negocio para insertar un Proveedor
        /// </summary>
        /// <param name="Proveedor"></param>
        public static void Insert(Proveedor proveedor)
        {
            Operaciones.WriteLogsDebug("ProveedorBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un Proveedor"));

            try
            {
                ProveedorDal.Insert(proveedor);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar Proveedor"));

        }

        /// <summary>
        /// método lógica de negocio para actulizar un Proveedor
        /// </summary>
        /// <param name="Proveedor"></param>
        public static void Update(Proveedor proveedor)
        {
            Operaciones.WriteLogsDebug("ProveedorBrl", "Actualizar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un Proveedor"));

            try
            {
                ProveedorDal.Update(proveedor);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para actualizar Proveedor"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un Proveedor
        /// </summary>
        /// <param name="Proveedor"></param>
        public static void SoftDelete(int id)
        {
            Operaciones.WriteLogsDebug("ProveedorBrl", "Eliminar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Eliminar un Proveedor"));

            try
            {
                ProveedorDal.SoftDelete(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProveedorBrl", "Eliminar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Eliminar Proveedor"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un Proveedor
        /// </summary>
        /// <param name="Proveedor"></param>
        public static Proveedor Get(int id)
        {
            Operaciones.WriteLogsDebug("ProveedorBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un Proveedor"));

            try
            {
                return ProveedorDal.Get(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
    }
}
