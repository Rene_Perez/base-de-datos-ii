﻿using GameStore.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProveedorDAL;

namespace ProveedorBRL
{
    public class ProveedorKeyValueListBrl
    {
        public static ProveedorKeyValueList Get(string nombre)
        {
            Operaciones.WriteLogsDebug("ProveedorKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un ProveedorKeyValueList"));

            try
            {
                 return ProveedorKeyValueListDal.Get(nombre);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProveedorKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProveedorKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
    }
}
