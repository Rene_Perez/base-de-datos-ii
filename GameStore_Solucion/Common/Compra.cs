﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class Compra
    {
        public Guid IdCompra { get; set; }
        public SqlMoney TotalCompra { get; set; }
        public DateTime Fecha { get; set; }
        public Guid IdEmpleado { get; set; }
        public int IdProveedor { get; set; }
        public List<DetalleCompra> DetalleCompra { get; set; }
    }
}
