﻿using System;
using System.Data.SqlTypes;

namespace GameStore.Common
{
    public class Producto
    {
        #region Atributos y Propiedades

        public int IdProducto { get; set; }
        public string Imagen { get; set; }        
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public byte Tipo { get; set; }
        public SqlMoney Precio { get ; set ; }
        public int Stock { get; set; }
        public byte Estado { get; set; }

        #endregion

        #region Constructores
       
        #endregion

        #region Metodos

        #endregion
    }
}
