﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class DetalleCompra
    {
        public Guid IdCompra { get; set; }
        public int CantidadProducto { get; set; }
        public SqlMoney PrecioUnitario { get; set; }        
        public int IdProducto { get; set; }
    }
}
