﻿using System.Data.SqlTypes;

namespace GameStore.Common
{
    public class Empleado:Persona
    {
        #region Atributos y Propiedades

        public SqlMoney Sueldo { get; set; }
        public byte Cargo { get; set; }

        #endregion
    }
}
