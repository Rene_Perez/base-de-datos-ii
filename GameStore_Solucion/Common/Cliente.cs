﻿using System;

namespace GameStore.Common
{
    /// <summary>
    /// Clase hija que sirve para crear clientes
    /// </summary>
    public class Cliente:Persona
    {
        #region Atributos y Propiedades

        public short Puntos { get; set; }

        #endregion
        
    }
}
