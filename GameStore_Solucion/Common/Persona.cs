﻿using System;

namespace GameStore.Common
{
    public class Persona
    {
        #region Atributos y Propiedades       

        public Guid IdPersona { get; set; }
        public Usuario Usuario { get; set; }

        public string Ci { get; set; }        
        public string Nombres { get; set; }
        public string PrimerApellido { get; set; }
        public string SegundoApellido { get; set; }
        public byte Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Direccion { get; set; }
        public byte Estado { get; set; }

        #endregion   
    }
}
