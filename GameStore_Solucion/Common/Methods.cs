﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class Methods
    {
        private static string connectionString = ConfigurationManager.ConnectionStrings["BDD_GameStore_ConectionStrig"].ConnectionString;
        public static SqlCommand CreateBasicCommand()
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand();
            cmd.Connection = connection;
            return cmd;
        }
        public static SqlCommand CreateBasicCommand(string query)
        {
            SqlConnection connection = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query);
            cmd.Connection = connection;
            return cmd;
        }
        public static List<SqlCommand> CreateNBasicCommand(int n)
        {
            List<SqlCommand> res = new List<SqlCommand>();
            //Creamos la conexion
            SqlConnection connection = new SqlConnection(connectionString);

            for (int i = 0; i < n; i++)
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = connection;
                res.Add(cmd);
            }
            return res;
        }

        #region Ejecucion de SqlCommand

        public static void ExecuteBasicCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public static void ExecuteBasicCommand(SqlCommand cmd, string query)
        {
            try
            {
                cmd.CommandText = query;
                cmd.Connection.Open();
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public static void ExecuteNBasicCommand(List<SqlCommand> lista)
        {
            SqlTransaction tran = null;

            try
            {
                lista[0].Connection.Open();
                tran = lista[0].Connection.BeginTransaction();
                lista[0].Transaction = tran;
                for (int i = 0; i < lista.Count; i++)
                {
                    lista[i].Transaction = tran;
                    lista[i].ExecuteNonQuery();
                }
                //NO existe error
                tran.Commit();
            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                lista[0].Connection.Close();
            }
        }
        public static int GetIDGenerateTable(string tabla)
        {
            int res = -1;
            string query = "SELECT IDENT_CURRENT('" + tabla + "')+IDENT_INCR('" + tabla + "')";
            try
            {
                SqlCommand cmd = CreateBasicCommand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return res;
        }
        public static int GetMaxID(string id, string tabla)
        {
            int res = -1;
            // string query = "SELECT MAX("+id+") FROM" + tabla;
            string query = "SELECT MAX(" + id + ") FROM " + tabla;
            try
            {
                SqlCommand cmd = CreateBasicCommand(query);
                res = int.Parse(ExecuteScalarCommand(cmd));
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }
        public static string ExecuteScalarCommand(SqlCommand cmd)
        {
            try
            {
                cmd.Connection.Open();
                return cmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
        }
        public static void Execute2BasicCommand(SqlCommand cmd1, SqlCommand cmd2)
        {
            SqlTransaction tran = null;
            try
            {
                cmd1.Connection.Open();
                tran = cmd1.Connection.BeginTransaction();
                cmd1.Transaction = tran;
                cmd1.ExecuteNonQuery();

                cmd2.Transaction = tran;
                cmd2.ExecuteNonQuery();

                //NO existe error
                tran.Commit();

            }
            catch (Exception ex)
            {
                tran.Rollback();
                throw ex;
            }
            finally
            {
                cmd1.Connection.Close();
            }
        }
        public static DataTable ExecuteDataTableCommand(SqlCommand cmd)
        {
            DataTable res = new DataTable();
            try
            {
                cmd.Connection.Open();
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.Fill(res);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
        public static SqlDataReader ExecuteDataReaderCommand(SqlCommand cmd)
        {
            SqlDataReader res = null;
            try
            {
                cmd.Connection.Open();
                res = cmd.ExecuteReader();
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return res;
        }

        #endregion
    }
}
