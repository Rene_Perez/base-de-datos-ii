﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.Common
{
    public class ClienteKeyValue
    {
        public Guid IdPersona { get; set; }
        public string Ci { get; set; }
        public string NombreCompleto { get; set; }       
        public DateTime FechaNacimiento { get; set; }        
        public byte Estado { get; set; }
        public short Puntos { get; set; }
    }
}
