﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameStore.Common;
using GameStore.DAL_Producto;

namespace GameStore.BRL_Producto
{
    public class ProductoKeyValueListBrl
    {
        public static ProductoKeyValueList Get(string nombre)
        {
            Operaciones.WriteLogsDebug("ProductoKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un ProductoKeyValueList"));

            try
            {
                return ProductoKeyValueListDal.Get(nombre);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
    }
}
