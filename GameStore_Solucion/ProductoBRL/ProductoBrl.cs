﻿using System;
using System.Collections.Generic;
using GameStore.DAL_Producto;
using GameStore.Common;
using System.Data.SqlClient;

namespace GameStore.BRL_Producto
{
    public class ProductoBrl
    {

        /// <summary>
        /// método lógica de negocio para insertar un Producto
        /// </summary>
        /// <param name="Producto"></param>
        public static void Insert(Producto producto)
        {
            Operaciones.WriteLogsDebug("ProductoBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un Producto"));

            try
            {
                ProductoDal.Insert(producto);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar Producto"));

        }

        /// <summary>
        /// método lógica de negocio para actulizar un Producto
        /// </summary>
        /// <param name="Producto"></param>
        public static void Update(Producto producto)
        {
            Operaciones.WriteLogsDebug("ProductoBrl", "Actualizar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un Producto"));

            try
            {
                ProductoDal.Update(producto);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Actualizar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoBrl", "Actualizar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para actualizar Producto"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un Producto
        /// </summary>
        /// <param name="Producto"></param>
        public static void SoftDelete(int id)
        {
            Operaciones.WriteLogsDebug("ProductoBrl", "Eliminar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Eliminar un Producto"));

            try
            {
                ProductoDal.SoftDelete(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Eliminar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoBrl", "Eliminar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para Eliminar Producto"));
        }

        /// <summary>
        /// método lógica de negocio para eliminar un Producto
        /// </summary>
        /// <param name="Producto"></param>
        public static Producto Get(int id)
        {
            Operaciones.WriteLogsDebug("ProductoBrl", "Obtener", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para Obtener un Producto"));

            try
            {
                return ProductoDal.Get(id);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }


        #region Operaciones CRUD       

        //Select
        /*
        public static List<Producto> Select()
        {
            try
            {
                return ProductoDal.Select();
            }
            catch (Exception)
            {
                throw;
            }
        }
        */
        #endregion
    }
}
