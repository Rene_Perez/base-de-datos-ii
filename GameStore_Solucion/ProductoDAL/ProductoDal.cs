﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using GameStore.Common;
using System.Data.SqlTypes;

namespace GameStore.DAL_Producto
{
    public class ProductoDal
    {

        /// <summary>
        /// Inserta un Usuario a la base de datos 
        /// </summary>
        /// <param name="Producto"></param>
        public static void Insert(Producto producto)
        {
            Operaciones.WriteLogsDebug("ProductoDal", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para crear un Producto"));

            SqlCommand command = null;

            //Consulta para insertar Productos
            string queryString = @"INSERT INTO Producto(nombre,descripcion,imagen,precio,stock,tipo,estado)
                             VALUES(@nombre,@descripcion,@imagen,@precio,@stock,@tipo,@estado)";

            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                
                command.Parameters.AddWithValue("@nombre", producto.Nombre);
                command.Parameters.AddWithValue("@descripcion", producto.Descripcion);
                command.Parameters.AddWithValue("@imagen", producto.Imagen);
                command.Parameters.AddWithValue("@precio", producto.Precio);               
                command.Parameters.AddWithValue("@stock", producto.Stock);
                command.Parameters.AddWithValue("@tipo", producto.Tipo);
                command.Parameters.AddWithValue("@estado", 1);

                OperacionesSql.ExecuteBasicCommand(command);

            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Insertar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoDal", "Insertar", string.Format("{0} {1} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el metodo acceso a datos para insertar Producto"));
        }


        /// <summary>
        /// Actualiza Usuario de la base de datos
        /// </summary>
        /// <param name="Producto"></param>
        public static void Update(Producto producto)
        {
            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Producto"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Producto SET nombre=@nombre,descripcion=@descripcion,imagen=@imagen,precio=@precio,tipo=@tipo
                             WHERE idProducto=@idProducto";
            try
            {

                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idProducto", producto.IdProducto);
                command.Parameters.AddWithValue("@nombre", producto.Nombre);
                command.Parameters.AddWithValue("@descripcion", producto.Descripcion);
                command.Parameters.AddWithValue("@imagen", producto.Imagen);
                command.Parameters.AddWithValue("@precio", producto.Precio);                
                command.Parameters.AddWithValue("@tipo", producto.Tipo);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Actualizar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoDal", "Actualizar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }

        

        /// <summary>
        /// Elimina Usuario de la base de datos
        /// </summary>
        /// <param name="idProducto"></param>
        public static void SoftDelete(int idProducto)
        {
            Operaciones.WriteLogsDebug("ProductoDal", "Eliminar", string.Format("{0} Info: {1}", DateTime.Now.ToString(), "Empezando a ejecutar el metodo acceso a datos para eliminar un Producto"));

            SqlCommand command = null;

            // Proporcionar la cadena de consulta 
            string queryString = @"UPDATE Producto SET estado=0
                                    WHERE idProducto = @idProducto";
            try
            {
                command = OperacionesSql.CreateBasicCommand(queryString);
                command.Parameters.AddWithValue("@idProducto", idProducto);
                OperacionesSql.ExecuteBasicCommand(command);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Eliminar", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("ProductoDal", "Eliminar", string.Format("{0} {1} Info: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), "Termino de ejecutar  el metodo acceso a datos para Eliminar un Cliente"));
        }


        public static Producto Get(int id)
        {
            Producto res = new Producto();
            SqlCommand cmd = null;
            SqlDataReader dr = null;
            string query = @"SELECT idProducto,nombre,descripcion,imagen,tipo,precio,stock
                           FROM Producto 
                           WHERE idProducto=@id AND estado = 1";
            try
            {
                cmd = OperacionesSql.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@id", id);
                dr = OperacionesSql.ExecuteDataReaderCommand(cmd);
                while (dr.Read())
                {
                    res = new Producto
                    {
                        IdProducto = id,
                        Nombre = dr[1].ToString(),
                        Descripcion = dr[2].ToString(),
                        Imagen = dr[3].ToString(),
                        Tipo = byte.Parse(dr[4].ToString()),
                        Precio = SqlMoney.Parse(dr[5].ToString()),
                        Stock = dr.GetInt32(6)
                    };
                }
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ProductoDal", "Obtenet(Get)", string.Format("{0} {1} Error: {1}", DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
            }
            return res;
        }
       /* #region Operaciones CRUD

        //Get
        public static Producto Get(int idProducto)
        {
            Producto producto = null;
            SqlCommand cmd = null;
            SqlDataReader sqlDataReader = null;

            string query = @"SELECT idProducto,imagenProducto,videoProducto,nombre,descripcion,tipo,precio,stock
                           FROM Producto 
                           WHERE idProducto=@idProducto AND estado = 1";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idProducto", idProducto);
                sqlDataReader = Methods.ExecuteDataReaderCommand(cmd);

                while (sqlDataReader.Read())
                {
                    producto = new Producto
                    {
                        IdProducto = sqlDataReader.GetInt32(0),
                        ImagenProducto = sqlDataReader[1].ToString(),
                        VideoProducto = sqlDataReader[2].ToString(),
                        Nombre = sqlDataReader[3].ToString(),
                        Descripcion = sqlDataReader[4].ToString(),
                        Tipo = short.Parse(sqlDataReader[5].ToString()),
                        Precio = SqlMoney.Parse(sqlDataReader[6].ToString()),
                        Stock = sqlDataReader.GetInt32(7)
                       
                    };
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                sqlDataReader.Close();
            }
            return producto;

        }

        //Get
        public static Producto GetStock(int idProducto)
        {
            Producto producto = null;
            SqlCommand cmd = null;
            SqlDataReader sqlDataReader = null;

            string query = @"SELECT idProducto,stock
                           FROM Producto 
                           WHERE idProducto=@idProducto AND estado = 1";
            try
            {
                cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idProducto", idProducto);
                sqlDataReader = Methods.ExecuteDataReaderCommand(cmd);

                while (sqlDataReader.Read())
                {
                    producto = new Producto
                    {
                        IdProducto = sqlDataReader.GetInt32(0),
                        Estado = sqlDataReader.GetByte(1)
                    };
                        
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                cmd.Connection.Close();
                sqlDataReader.Close();
            }
            return producto;

        }

        //Select
        public static List<Producto> Select()
        {
            List<Producto> productos = new List<Producto>();

            string query = @"SELECT idProducto,imagenProducto,videoProducto,nombre,descripcion,tipo,precio,stock
                           FROM Producto 
                           WHERE estado = 1";

            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                SqlDataReader sqlDataReader = Methods.ExecuteDataReaderCommand(cmd);
                while (sqlDataReader.Read())
                {
                    Producto producto = new Producto
                    {
                        IdProducto = sqlDataReader.GetInt32(0),
                        ImagenProducto = sqlDataReader[1].ToString(),
                        VideoProducto = sqlDataReader[2].ToString(),
                        Nombre = sqlDataReader[3].ToString(),
                        Descripcion = sqlDataReader[4].ToString(),
                        Tipo = short.Parse(sqlDataReader[5].ToString()),
                        Precio = SqlMoney.Parse(sqlDataReader[6].ToString()),
                        Stock = sqlDataReader.GetInt32(7)

                    };

                    productos.Add(producto);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return productos;
        }

        //INSERTAR
        public static void Insert(Producto producto)
        {
            string query = @"INSERT INTO Producto (imagenProducto,videoProducto,nombre,descripcion,tipo,precio,stock)
                             VALUES(@imagenProducto,@videoProducto,@nombre,@descripcion,@tipo,@precio,@stock)";

            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@imagenProducto", producto.ImagenProducto);
                cmd.Parameters.AddWithValue("@videoProducto", producto.VideoProducto);
                cmd.Parameters.AddWithValue("@nombre", producto.Nombre);
                cmd.Parameters.AddWithValue("@descripcion", producto.Descripcion);
                cmd.Parameters.AddWithValue("@tipo", producto.Tipo);
                cmd.Parameters.AddWithValue("@precio", producto.Precio);
                cmd.Parameters.AddWithValue("@stock", producto.Stock);

                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //ACTUALIZAR
        public static void Update(Producto producto)
        {
            string query = @"UPDATE Producto
                             SET imagenProducto=@imagenProducto,videoProducto=@videoProducto,nombre=@nombre,descripcion=@descripcion,tipo=@tipo,precio=@precio
                             WHERE idProducto=@idProducto";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idProducto", producto.IdProducto);

                cmd.Parameters.AddWithValue("@imagenProducto", producto.ImagenProducto);
                cmd.Parameters.AddWithValue("@videoProducto", producto.VideoProducto);
                cmd.Parameters.AddWithValue("@nombre", producto.Nombre);
                cmd.Parameters.AddWithValue("@descripcion", producto.Descripcion);
                cmd.Parameters.AddWithValue("@tipo", producto.Tipo);
                cmd.Parameters.AddWithValue("@precio", producto.Precio);

                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //ELIMINADO LOGICO
        public static void SoftDelete(int idProducto)
        {
            string query = "UPDATE Producto " +
                           "SET estado=0 " +
                           "WHERE idProducto=@idProducto";
            try
            {
                SqlCommand cmd = Methods.CreateBasicCommand(query);
                cmd.Parameters.AddWithValue("@idPersona", idProducto);
                Methods.ExecuteBasicCommand(cmd);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion*/
    }
}
