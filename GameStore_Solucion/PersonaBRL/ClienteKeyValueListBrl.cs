﻿using GameStore.Common;
using GameStore.DAL_Persona;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameStore.BRL_Persona
{
    public class ClienteKeyValueListBrl
    {
        public static ClienteKeyValueList Get(string nombre)
        {
            Operaciones.WriteLogsDebug("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Info: {1}",
             DateTime.Now.ToString(),
             "Empezando a ejecutar el método lógica de negocio para Obtener un ClienteKeyValueList"));

            try
            {
                return ClienteKeyValueListDal.Get(nombre);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("ClienteKeyValueListBrl", "Obtener", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

        }
    }
}
