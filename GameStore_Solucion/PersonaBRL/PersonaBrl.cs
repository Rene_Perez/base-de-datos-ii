﻿using System;
using System.Collections.Generic;
using GameStore.DAL_Persona;
using GameStore.Common;
using System.Data.SqlClient;

namespace GameStore.BRL_Persona
{
    public class PersonaBrl
    {
        /// <summary>
        /// método lógica de negocio para insertar un persona
        /// </summary>
        /// <param name="persona"></param>
        public static void Insert(Persona persona)
        {
            Operaciones.WriteLogsDebug("PersonaBrl", "Insertar", string.Format("{0} Info: {1}",
            DateTime.Now.ToString(),
            "Empezando a ejecutar el método lógica de negocio para crear un persona"));

            try
            {
                PersonaDal.Insertar(persona);
            }
            catch (SqlException ex)
            {
                Operaciones.WriteLogsRelease("PersonaBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }
            catch (Exception ex)
            {
                Operaciones.WriteLogsRelease("PersonaBrl", "Insertar", string.Format("{0} Error: {1}",
                    DateTime.Now.ToString(), DateTime.Now.ToString(), ex.Message));
                throw ex;
            }

            Operaciones.WriteLogsDebug("PersonaBrl", "Insertar", string.Format("{0} Info: {1}",
                DateTime.Now.ToString(), DateTime.Now.ToString(),
                "Termino de ejecutar  el método lógica de negocio para insertar persona"));

        }
    }
}
